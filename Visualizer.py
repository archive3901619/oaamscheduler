import networkx as nx
import matplotlib.pyplot as plt
import numpy as np


class Visualizer:

    def __init__(self, avionics_model):

        self.__avionics_model = avionics_model

        # load the model if not yet happened
        if not self.__avionics_model.is_loaded():
            self.__avionics_model.load()

        # read some basic information
        self.__t_n = self.__avionics_model.get_task_count()
        self.__d_n = self.__avionics_model.get_device_count()

        self.__tasks = self.__avionics_model.get_task_list()
        self.__devices = self.__avionics_model.get_device_list()

    def show_dependency(self):

        # shows the dependency of the avionics model as a graph
        # nodes are tasks
        # edges connect tasks if they have a precedence realtionship

        # get necessary information
        adj = self.__avionics_model.get_precedence_matrix()
        rates = self.__avionics_model.get_task_rates()
        task_assignment = self.__avionics_model.get_task_assignment()
        signal_matrix = self.__avionics_model.get_signal_matrix()

        # create Graph
        G = nx.DiGraph()

        rows, cols = np.where(adj > 0)
        edges = zip(rows.tolist(), cols.tolist())

        pos = self.__get_node_position(adj)
        pos_dict = dict(zip([i for i in range(self.__t_n)], pos))
        labels_dict = dict(zip([i for i in range(self.__t_n)], [str(i) for i in range(self.__t_n)]))

        signal_names = []

        # create name for edges
        for edge in list(edges):
            signal_names.append(str(signal_matrix[edge[0]][edge[1]] * (task_assignment[edge[0]] != task_assignment[edge[1]])) + " s")

        edges = zip(rows.tolist(), cols.tolist())
        G.add_nodes_from([i for i in range(self.__t_n)])
        G.add_edges_from(edges)

        options = {"node_size": 300, "alpha": 0.9}
        nx.draw_networkx_nodes(G, pos_dict, node_color="#163254", **options)
        nx.draw_networkx_edges(G, pos_dict, width=1.0, alpha=0.9)
        nx.draw_networkx_labels(G, pos_dict, labels_dict, font_size=8, font_color="#FFFFFF")

        # create labels for each task
        for i in range(self.__t_n):
            device_name = "No Device" if task_assignment[i] == -1 else self.__avionics_model.get_name(self.__devices[int(task_assignment[i])])
            name = self.__avionics_model.get_name(self.__tasks[i])
            name += "\n(" + device_name + "@"+str(rates[i])+"Hz)"
            plt.text(pos[i][0], pos[i][1] + 0.05 * max([pos[i][1] for i in range(len(pos))]), s=name, horizontalalignment='center')

        plt.title("Dependency graph \n " + self.__avionics_model.get_model_name())
        plt.box(False)
        plt.show()

    def show_schedule(self, t_end, show_job_number=False):

        # show the schedule from 0 to t_end as a plot
        # show_job_number = True shows the number of the instance of a tasks

        # get necessary information
        task_assignment = self.__avionics_model.get_task_assignment()
        schedule = self.__avionics_model.get_schedule()
        offsets = [schedule[i][0] for i in range(self.__t_n)]
        rates = [schedule[i][1] for i in range(self.__t_n)]
        wcets = [schedule[i][2] for i in range(self.__t_n)]

        tasks_sorted = np.argsort(offsets)

        cmap = plt.get_cmap("tab10")

        # plotting the periodic starting times of all given tasks
        for i in range(self.__t_n):

            if task_assignment[i] != -1:

                last_end_time = 0
                x_values = np.array([0])
                y_values = np.array([i*2])

                for k in range(int(t_end * rates[tasks_sorted[i]] / 1000) + 2):

                    real_start_time = (offsets[tasks_sorted[i]] + k / rates[tasks_sorted[i]]) * 1000
                    real_end_time = (offsets[tasks_sorted[i]] + k / rates[tasks_sorted[i]] + wcets[tasks_sorted[i]]) * 1000

                    # add one instance of the tasks
                    x_values = np.append(x_values, [last_end_time, real_start_time, real_start_time, real_end_time, real_end_time])
                    y_values = np.append(y_values, [i*2, i*2, i*2+0.5, i*2+0.5, i*2])

                    last_end_time = real_end_time

                    # show job number of tasks
                    if show_job_number and (real_end_time + real_start_time) / 2 < t_end:
                        plt.text((real_end_time + real_start_time) / 2, i*2 + 0.7, str(k), color=cmap(i % 10), horizontalalignment="center")

                plt.plot(x_values, y_values, color=cmap(i % 10))

            else:
                plt.plot([0, t_end], [i * 2, i * 2], color=cmap(i))

        task_names = []

        # add labels to the tasks
        for i in tasks_sorted:
            frequency_text = str(np.round(rates[i], 0)) + "Hz"
            device_name = "No Device" if task_assignment[i] == -1 else self.__avionics_model.get_name(self.__devices[int(task_assignment[i])])
            task_names.append(self.__avionics_model.get_name(self.__tasks[i]) + "\n (" + device_name + "@" + frequency_text + ")")

        plt.title("Schedule \n" + self.__avionics_model.get_model_name())
        plt.xlabel("time [ms]")
        plt.ylabel("Tasks")
        plt.xlim(-t_end / 100, t_end * 1.01)
        plt.ylim(-0.5, 2 * self.__t_n + 0.5)
        plt.yticks([2*i for i in range(self.__t_n)], task_names)

        plt.grid(b=True, axis="x", which="major", color="#BBBBBB", linestyle="-")
        plt.grid(b=True, axis="x", which="minor", color="#CCCCCC", linestyle="--")
        plt.minorticks_on()

        plt.show()

    # PRIVATE METHODS

    def __get_node_position(self, adj, scale_x=1, scale_y=1):

        # get all starting nodes
        starting_nodes = [i for i in range(self.__t_n) if np.sum(np.transpose(adj)[i]) == 0]

        # estimate max_width
        max_width = int(self.__t_n ** 0.5)

        query = starting_nodes
        graph_offset_y = 0
        graph_offset_x = 0
        width_counter = 0
        current_branch_max = 0
        pos = [[-1, -1]] * self.__t_n

        # propergate through every process chain
        while query:

            levels = [-1] * self.__t_n
            branch = []

            self.__progergate_graph(adj, query[0], 0, levels, branch)

            branch_max = (max([max(i) for i in branch]) + 1)
            branch_mean = branch_max / 2

            for i in range(self.__t_n):
                if levels[i] != -1:
                    pos[i] = [(levels[i] + graph_offset_x) * scale_x, (branch[levels[i]][i] + branch_mean - (max(branch[levels[i]]) + 1) / 2 + graph_offset_y) * scale_y]

            if branch_max > current_branch_max:
                current_branch_max = branch_max

            width_counter += max(levels) + 1

            if width_counter >= max_width:
                graph_offset_y += current_branch_max
                graph_offset_x = 0
                width_counter = 0
            else:
                graph_offset_x += max(levels) + 1

            query.remove(query[0])

            for q in query:
                if levels[q] != -1:
                    query.remove(q)

        return pos

    def __progergate_graph(self, A, node, level, levels, branch):

        if levels[node] == -1:

            levels[node] = level

            # create new branches if necessary
            if len(branch) <= level:
                branch.append([-1] * self.__t_n)

            # set the branch
            branch[level][node] = max(branch[level]) + 1

            # repeat for next branches
            for i in range(self.__t_n):
                if A[node][i] > 0:
                    self.__progergate_graph(A, i, level + 1, levels, branch)
                elif A[i][node] > 0:
                    self.__progergate_graph(A, i, level - 1, levels, branch)
