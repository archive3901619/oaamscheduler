from Scheduler import Scheduler
from AvionicsModel import AvionicsModel
from Visualizer import Visualizer
import time

# ----------------------------------
#        EXAMPLE SCRIPT            |
# ----------------------------------

# Use this as a guide for calling the scheduler

if __name__ == '__main__':

    # Different models available for testing:
    # - Workspace/DualRateSensor.oaam - Two Sensor with different non harmonic frequencies
    # - Workspace/MonitorExample.oaam - Simple controller monitor setup with cyclic dependency
    # - Workspace/SimpleBAS.oaam - Example for Bleed Air System
    # - Workspace/SimpleBAS_Extended.oaam - Example of BAS for dynamic scheduling
    # - Workspace/SimpleFlightControl.oaam - Small Flight Control consisting of three redundant controllers
    # - Workspace/Test"x".oaam (x=20,40,80,100) - Generated test example with x functions

    model_file_path = 'Workspace/SimpleBAS.oaam'
    meta_file_path = 'Workspace/Meta/oaam.ecore'

    # create a model instance to work with
    avionics_model = AvionicsModel(model_file_path , meta_file_path)
    avionics_model.load()

    # determine allocation of task to devices and schedule
    scheduler = Scheduler(avionics_model)
    status, task_assignment, schedule = scheduler.schedule(print_out=True, optimality=0.8,time_limit=200)

    # write results of scheduler into the model
    avionics_model.write_task_assignment(task_assignment)
    avionics_model.write_schedule(schedule)

    # visualize the results
    visualizer = Visualizer(avionics_model)
    visualizer.show_schedule(t_end=430, show_job_number=True)