import numpy as np
import os

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider, PyEcoreMdbAccessor, PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain
from eoq2 import *


class AvionicsModel:

    def __init__(self, model_file_path, meta_file_path):

        # get name model from file name
        self.__model_name = os.path.splitext(os.path.basename(model_file_path))[0]

        # create access to the model
        self.__mdbProvider = PyEcoreSingleFileMdbProvider(model_file_path, meta_file_path)
        self.__valueCodec = PyEcoreIdCodec()

        self.__mdbAccessor = PyEcoreMdbAccessor(self.__mdbProvider.GetMdb(), self.__valueCodec)
        self.__domain = LocalMdbDomain(self.__mdbAccessor)
        self.__mdbProvider.CoupleWithDomain(self.__domain, self.__valueCodec)

        # loading flag
        self.__loaded = False

        # init empty values for all lists
        self.__tasks_qry = []
        self.__t_n = 0
        self.__tasks_dic = {}

        self.__devices_qry = []
        self.__d_n = 0
        self.__devices_dic = {}

        self.__capability_qry = []
        self.__c_n = 0

        self.__resource_types_qry = []
        self.__r_n = 0
        self.__resources_dic = {}

        self.__connection_types_qry = []
        self.__connection_n = 0
        self.__connections_dic = {}

        self.__signal_types_qry = []
        self.__signal_n = 0
        self.__signal_types_dic = {}
        self.__signal_times = []

        self.__signal_capabilites = []
        self.__signal_cap_n = 0
        self.__signal_capabilites_dic = {}

        self.__atomics_qry = []
        self.__segregations_qry = []

        self.__capabilities = []
        self.__precedence_matrix = []
        self.__signal_matrix = []
        self.__connections = []
        self.__rates = []
        self.__wcets = []

        self.__task_assignment = []
        self.__schedule = []

    def load(self):

        # only load if not loaded yet
        if self.__loaded:
            return

        # load the model to read information necessary for task allocation and scheduling
        # when loaded then loading flag is set to true
        self.__loaded = True

        # read all tasks
        self.__tasks_qry = self.__domain.Do(Get(Cls('Task')))
        self.__t_n = len(self.__tasks_qry)

        # create dictionary for quick access (hashing)
        self.__tasks_dic = {k: v for k, v in zip([str(task_qry) for task_qry in self.__tasks_qry], [i for i in range(self.__t_n)])}

        # read all devices
        self.__devices_qry = self.__domain.Do(Get(Cls('Device')))
        self.__d_n = len(self.__devices_qry)
        self.__devices_dic = {k: v for k, v in zip([str(device_qry) for device_qry in self.__devices_qry], [i for i in range(self.__d_n)])}

        # read all constraints
        self.__atomics_qry = self.__domain.Do(Get(Cls('TaskAtomicRestriction')))
        self.__segregations_qry = self.__domain.Do(Get(Cls('SegregationRestriction')))

        # read all capabilites
        self.__capability_qry = self.__domain.Do(Get(Cls('TaskOnDeviceCapability')))
        self.__c_n = len(self.__capability_qry)

        # read available resources
        self.__resource_types_qry = self.__domain.Do(Get(Cls('ResourceType')))
        self.__r_n = len(self.__resource_types_qry)
        self.__resources_dic = {k: v for k, v in zip([str(resource_qry) for resource_qry in self.__resource_types_qry], [i for i in range(self.__d_n)])}

        # read all connection types and signal types
        self.__connection_types_qry = self.__domain.Do(Get(Cls('ConnectionType')))
        self.__connection_n = len(self.__connection_types_qry)
        self.__connections_dic = {k: v for k, v in zip([str(connection_type) for connection_type in self.__connection_types_qry], [i + 1 for i in range(self.__connection_n)])}

        self.__signal_types_qry = self.__domain.Do(Get(Cls('SignalType')))
        self.__signal_n = len(self.__signal_types_qry)
        self.__signal_types_dic = {k: v for k, v in zip([str(signal_type) for signal_type in self.__signal_types_qry], [i + 1 for i in range(self.__signal_n)])}

        # read all signal capabilites
        self.__signal_capabilites = self.__domain.Do(Get(Cls('SignalOnConnectionOrDeviceCapability')))
        self.__signal_cap_n = len(self.__signal_capabilites)
        self.__signal_capabilites_dic = {k: v for k, v in zip([str(cap) for cap in self.__signal_capabilites], [i for i in range(self.__signal_cap_n)])}

        # determine capabilites
        self.__capabilities = self.__get_capabilities()

        # determine signal transmission times between functions and precedence realtionsships
        self.__signal_matrix, self.__precedence_matrix = self.__get_signal_matrix()
        self.__connections = self.__get_connections_matrix()
        self.__signal_times = self.__get_signal_times()

        # find and mark cycles
        self.__precedence_matrix = self.__find_cycles(self.__precedence_matrix)

        # determine rates with rate propergation
        self.__rates = self.__get_task_rates(self.__precedence_matrix)

        # determine wcet and utilization
        self.__wcets = self.__get_wcet_matrix()

        # read existing task assignment and schedule
        self.__task_assignment, self.__schedule = self.__get_schedule()

    def is_loaded(self):
        return self.__loaded

    # PUBLIC GETTERS
    # --------------

    def get_type(self, eoq_object):

        return self.__domain.Do(Get(Qry(eoq_object).Pth('type')))

    def get_name(self, eoq_object):

        return self.__domain.Do(Get(Qry(eoq_object).Pth('name')))

    def get_task_list(self):
        return self.__tasks_qry

    def get_device_list(self):
        return self.__devices_qry

    def get_segregation_pairs(self):

        # create list of segregated pairs of tasks
        segregation_pairs = []

        for s in self.__segregations_qry:

            # retrieve pair of segregated tasks
            taskA, taskB = self.__domain.Do(Get(Qry(s).Pth("tasksA")))[0], self.__domain.Do(Get(Qry(s).Pth("tasksB")))[0]
            taskA_i = self.__tasks_dic[str(taskA)]
            taskB_i = self.__tasks_dic[str(taskB)]
            segregation_pairs.append([taskA_i, taskB_i])

        return segregation_pairs

    def get_atomic_pairs(self):

        # create a list of atomic pairs of tasks
        atomic_pairs = []

        for a in self.__atomics_qry:

            # retrieve atomic tasks
            tasks = self.__domain.Do(Get(Qry(a).Pth("tasks")))

            # always select a pair of atomic tasks and put into list
            for i in range(len(tasks)):
                for j in range(i + 1, len(tasks)):
                    atomic_pairs.append([tasks[i], tasks[j]])

        return atomic_pairs

    def get_time_delay_restrictions(self):

        # create a list of 3 tuples with (start task , end task , max delay)
        time_delays = []

        # retrieve all time delay restictions
        time_delay_restrictions = self.__domain.Do(Get(Cls('TimeDelayRestriction')))

        # collect information on every time delay restriction
        for td in time_delay_restrictions:

            # collect start and end task and delay
            start_task = self.__domain.Do(Get(Qry(td).Pth('startTask')))
            end_task = self.__domain.Do(Get(Qry(td).Pth('endTask')))
            delay = self.__domain.Do(Get(Qry(td).Pth('delay')))

            # append 3 tuple to list
            time_delays.append([self.__tasks_dic[str(start_task)], self.__tasks_dic[str(end_task)], delay])

        return time_delays

    def get_synchronicity_restrictions(self):

        # create a list of 3 tuples with (start_task , end_task , max jitter)
        synchro_list = []

        # retrieve all synchro restrictions
        synchros = self.__domain.Do(Get(Cls('SynchronicityRestriction')))

        # collect information from the synchronicity restriction
        for syn in synchros:

            tasks = self.__domain.Do(Qry(syn).Pth("tasks"))
            jitter = self.__domain.Do(Qry(syn).Pth("maxJitter"))
            task_list = [self.__tasks_dic[str(task)] for task in tasks]

            # always create pairs of two tasks
            if len(task_list) >= 2:
                for i in range(len(task_list)):
                    for j in range(i+1, len(task_list)):
                        synchro_list.append([i, j, jitter])

        return synchro_list

    def get_capabilities(self):

        return self.__capabilities

    def get_signal_matrix(self):

        return self.__signal_matrix

    def get_precedence_matrix(self):

        return self.__precedence_matrix

    def get_resource_type_count(self):
        return self.__r_n

    def get_resource_matrix(self):

        # create 3 dimensional matrix for resource consumption
        # An element with index i,d,r means resource consumption of task i on device d for resource r
        A_resource = np.zeros((self.__t_n, self.__d_n, self.__r_n))

        # create 2 dimensional matrix for resource capacity
        # An element with index d,r means availabel resource count of resource r on device d
        b_resource = np.zeros((self.__d_n, self.__r_n))

        # construct available resources
        # for all devices
        for d in range(self.__d_n):

            # collect all resources on device d
            device_resources = self.__domain.Do(Get(Qry(self.get_type(self.__devices_qry[d])).Cls('Resource').Trm()))

            # go through all resources
            if device_resources:
                for r in device_resources:

                    # receive type and count of resource
                    resource_count = self.__domain.Do(Get(Qry(r).Pth('count')))
                    resource_type = self.get_type(r)
                    resource_index = self.__resources_dic[str(resource_type)]

                    b_resource[d][resource_index] = resource_count

        # collect information on resource consumption
        for t in range(self.__t_n):
            for d in range(self.__d_n):
                if self.__capabilities[t * self.__d_n + d] > 0:

                    # get the capability of task i on device d
                    c = self.__capability_qry[self.__capabilities[t * self.__d_n + d]]
                    resources_c = self.__domain.Do(Get(Qry(c).Cls('ResourceConsumption').Trm()))

                    # if this capability has resource consumption then read it
                    if resources_c:
                        for r in resources_c:

                            # get type and count of resource consumption
                            r_i = self.__resources_dic[str(self.get_type(r))]
                            r_count = self.__domain.Do(Get(Qry(r).Pth('count')))

                            A_resource[t][d][r_i] = r_count

        return A_resource, b_resource

    def get_device_count(self):
        return self.__d_n

    def get_task_count(self):
        return self.__t_n

    def get_wcet_matrix(self):
        return self.__wcets

    def get_task_rates(self):
        return self.__rates

    def get_task_assignment(self):
        return self.__task_assignment

    def get_schedule(self):
        return self.__schedule

    def get_model_name(self):
        return self.__model_name

    def get_tasks_of_device(self, device_id):
        return [i for i in range(self.__t_n) if self.__task_assignment[i] == device_id]

    def get_connections(self, task_A, task_B):

        # create list of possible hardware connections
        connections = []

        # connections between two tasks are all pairs of modules that can host those tasks
        # in this version connections between two tasks dont need physical connections
        # physical connections are just extra information determining signal times
        for d1 in range(self.__d_n):
            if self.__capabilities[task_A * self.__d_n + d1] != -1:
                for d2 in range(self.__d_n):
                    if self.__capabilities[task_B * self.__d_n + d2] != -1:
                        connections.append([d1, d2])

        return connections

    def get_signal_time(self, task_A, task_B, device_A, device_B):

        # returns signal time depending on the tasks and modules they are placed on
        if device_A == device_B:
            return 0
        else:

            # retrieve type of signal and type of physical connection
            # if there is no physical connection then signal type will determine signal time
            signal_type = int(self.__signal_matrix[task_A][task_B])
            connection_type = int(self.__connections[device_A][device_B])

            return self.__signal_times[signal_type][connection_type]

    # LOADING METHODS
    # ---------------

    def __get_capabilities(self):

        # go through all capabilities
        A_capability = [-1] * (self.__t_n * self.__d_n)

        # read task types and device types of each capability
        capability_types = [(self.__domain.Do(Get(Qry(c).Pth('taskType'))), self.__domain.Do(Get(Qry(c).Pth('deviceType'))))
                            for c in self.__capability_qry]

        # go through each pair of task and device
        for i in range(self.__t_n * self.__d_n):

            # retrieve the types of task and device
            task_type = self.get_type(self.__tasks_qry[i // self.__d_n])
            device_type = self.get_type(self.__devices_qry[i % self.__d_n])

            # check for all capabilites if there is a matching capability
            # if yes then set index of capability as value
            for c in range(self.__c_n):
                if (task_type, device_type) == capability_types[c]:
                    A_capability[i] = c
                    break

        return A_capability

    def __get_wcet_matrix(self):

        # wcet - worst case execution time
        # 2 dimensional matrix - index [i][d] is the wcet of task i on device d
        wcet = np.zeros((self.__t_n, self.__d_n))

        # check every possible pair device and task and check if there is a capability
        for d in range(self.__d_n):
            for t in range(self.__t_n):
                if self.__capabilities[t*self.__d_n+d] >= 0:

                    # read wcet as attribute of wcet
                    c = self.__capability_qry[self.__capabilities[t*self.__d_n+d]]
                    wcet[t][d] = float(round(self.__domain.Do(Get(Qry(c).Pth("worstCaseExecutionTime"))), 6))

        return wcet

    def __get_task_rates(self, A):

        # create empty lists for rates and driving tasks
        rates = np.ones(self.__t_n) * -1
        driving_tasks = np.zeros(self.__t_n)

        # collect all fixed rates
        for t in range(self.__t_n):
            if self.__domain.Do(Get(Qry(self.__tasks_qry[t]).Pth('fixedRate'))) > 0:
                rates[t] = self.__domain.Do(Get(Qry(self.__tasks_qry[t]).Pth('fixedRate')))
                driving_tasks[t] = True

        # for every driving task -> propergate through graph
        for t in range(self.__t_n):
            if driving_tasks[t]:
                self.__propergate_diagraph(t, A, driving_tasks, rates, rates[t])
                self.__propergate_diagraph(t, np.transpose(A), driving_tasks, rates, rates[t])

        return rates

    def __propergate_diagraph(self, tid, A, driving_tasks, rates, rate):

        # propergate rate through depedency graph by applying a depth search
        # as long as there is neighbouring task, that doenst have a rate or a lower rate, this function will continue
        # nodes that are changed are the starting points for a new propergation
        for t in range(self.__t_n):
            if not driving_tasks[t] and A[tid][t] > 0 and (rates[t] < 0 or rate >= rates[t]):
                rates[t] = rate
                self.__propergate_diagraph(t, A, driving_tasks, rates, rate)

    def __get_signal_matrix(self):

        # retrieve the signal types and precedence relationships between two tasks
        # signal_matrix - 2 dimensional matrix, index [i][j] is the index of the signal type between task i and task j
        # precedence_matrix - 2 dimensional matrix, if index[i][j] = 1 then there is a precedence relationship
        # between task i and task j, else not
        signal_matrix = np.zeros((self.__t_n, self.__t_n))
        precedence_matrix = np.zeros((self.__t_n, self.__t_n))

        # find all signals
        signals = self.__domain.Do(Get(Cls('Signal')))

        for signal in signals:

            # get source, target and types of each signal
            source = self.__domain.Do(Get(Qry(signal).Pth('source').Met('CONTAINER')))
            targets = self.__domain.Do(Get(Qry(signal).Pth('targets').Met('CONTAINER')))
            signal_type = self.get_type(signal)

            for target in targets:
                precedence_matrix[self.__tasks_dic[str(source)]][self.__tasks_dic[str(target)]] = 1
                signal_matrix[self.__tasks_dic[str(source)]][self.__tasks_dic[str(target)]] = \
                    self.__signal_types_dic[str(signal_type)]

        return signal_matrix, precedence_matrix

    def __get_connections_matrix(self):

        # 2 dimensional matrix - index [d][e] is the index of the physical connection between module d and module e
        # if index [d][e] = 0 then there is no physical connection
        connection_matrix = np.zeros((self.__d_n, self.__d_n))

        # find all connections
        connections_qry = self.__domain.Do(Get(Cls("Connection")))

        for connection in connections_qry:

            # get source and target of connection and type
            starting_points = self.__domain.Do(Get(Qry(connection).Pth("startingPoints")))
            end_points = self.__domain.Do(Get(Qry(connection).Pth("endPoints")))
            connection_type = self.get_type(connection)

            for s in starting_points:
                for e in end_points:
                    source = self.__domain.Do(Get(Qry(s).Met('CONTAINER')))
                    target = self.__domain.Do(Get(Qry(e).Met('CONTAINER')))
                    connection_matrix[self.__devices_dic[str(source)]][self.__devices_dic[str(target)]] = \
                        self.__connections_dic[str(connection_type)]

        return connection_matrix

    def __get_signal_times(self):

        # 2 dimensional matrix - index [a][b] is the signal time of a signal type a and connection type b
        signal_times = np.zeros((self.__signal_n + 1, self.__connection_n + 1))

        for signal_cap in self.__signal_capabilites:

            # get types of signal capability
            signal_type = self.__domain.Do(Get(Qry(signal_cap).Pth("signalType")))
            connection_type = self.__domain.Do(Get(Qry(signal_cap).Pth("connectionType")))

            # if there is no signal type or connection type in the capability then set 0 as default
            signal_index = self.__signal_types_dic[str(signal_type)] if signal_type else 0
            connection_index = self.__connections_dic[str(connection_type)] if connection_type else 0

            signal_times[signal_index][connection_index] = self.__domain.Do(Get(Qry(signal_cap).Pth("worstCaseTransmissionTime")))

        return signal_times

    def __find_cycles(self, adj):

        status = [0] * self.__t_n

        # use depth seearch to find cycles
        # use status variable to determine topology
        # a node can have following status:
        #   - status = 0 : not started yet
        #   - status = 1 : in processing
        #   - status = 2 : finished

        for i in range(self.__t_n):
            if status[i] == 0:
                self.__depth_search(i, -1, adj, status)

        return adj

    def __depth_search(self, current, previous, adj, status):

        # returned to a node which was in processing -> backward edge
        if status[current] == 1:
            adj[previous][current] = 2

        # node not started yet
        elif status[current] == 0:

            # process node
            status[current] = 1

            # apply depth search to all neighbouring nodes
            for i in range(self.__t_n):
                if adj[current][i] > 0:
                    self.__depth_search(i, current, adj, status)

            # node is finished
            status[current] = 2

    def __get_schedule(self):

        # task_assignment - 1 dimensional list, index [i] is the index of the module, on which task i is assigned to
        # schedule - 2 dimensional list, index [i] is 3-Tuple with (offset , rate , wcet) of task i

        self.__task_assignment = [-1] * self.__t_n
        self.__schedule = [[-1, -1, -1]] * self.__t_n

        # reading the task assignment and assign each task the corresponding device index
        for t in self.__domain.Do(Get(Cls('TaskAssignment'))):

            task = self.__domain.Do(Get(Qry(t).Pth("task")))
            device = self.__domain.Do(Get(Qry(t).Pth("device")))

            wcet = self.__domain.Do(Get(Qry(t).Pth("capability").Pth("worstCaseExecutionTime")))
            rate = self.__domain.Do(Get(Qry(t).Pth("schedules").Trm().Pth("rate")))
            offset = self.__domain.Do(Get(Qry(t).Pth("schedules").Trm().Pth("scheduledTimes").Trm().Pth("startTime")))

            rate = -1 if not rate else rate[0]
            offset = -1 if not offset else offset[0][0]

            self.__task_assignment[self.__tasks_dic[str(task)]] = self.__devices_dic[str(device)]
            self.__schedule[self.__tasks_dic[str(task)]] = [offset, rate, wcet]

        return self.__task_assignment, self.__schedule

    # WRITING METHODS
    # ---------------

    def write_task_assignment(self, task_assignment):

        # set the new task assignment
        self.__task_assignment = task_assignment

        for i in range(len(self.__task_assignment)):

            # search for task assignment
            task_assignment_qry = self.__domain.Do(Get(Cls("TaskAssignment").Sel(Pth("task").Equ(Qry(self.__tasks_qry[i]))).Trm()))

            # if task assingment didnt exist yet then create a new one
            if not task_assignment_qry:

                # create new task assingment element
                task_assignment_qry = self.__domain.Do(Cmp().Crn("http://www.oaam.de/oaam/model/v140/allocations", "TaskAssignment", 1))

                # add task assignment to the model
                self.__domain.Do(Add(Pth('allocations'), 'taskAssignments', Qry(task_assignment_qry[0])))

            # set new attributes for task assingment
            self.__domain.Do(Set(Qry(task_assignment_qry[0]), 'name', self.get_name(self.__tasks_qry[i])))
            self.__domain.Do(Set(Qry(task_assignment_qry[0]), 'task', Qry(self.__tasks_qry[i])))
            self.__domain.Do(Set(Qry(task_assignment_qry[0]), 'device', Qry(self.__devices_qry[int(task_assignment[i])])))
            self.__domain.Do(Set(Qry(task_assignment_qry[0]), 'capability', Qry(self.__capability_qry[self.__capabilities[i * self.__d_n + int(task_assignment[i])]])))

    def write_schedule(self, schedule):

        # set the new schedule
        self.__schedule = schedule

        for i in range(len(self.__schedule)):

            # search for task assignment
            task_assignment_qry = self.__domain.Do(Get(Cls("TaskAssignment").Sel(Pth("task").Equ(Qry(self.__tasks_qry[i]))).Trm()))

            if task_assignment_qry:

                schedule_qry = self.__domain.Do(Get(Qry(task_assignment_qry[0]).Pth("schedules").Trm()))

                # if no schedule exist then create a new one
                if not schedule_qry:

                    schedule_qry = self.__domain.Do(Cmp().Crn("http://www.oaam.de/oaam/model/v140/allocations", "Schedule", 1))
                    self.__domain.Do(Add(Qry(task_assignment_qry[0]), 'schedules', Qry(schedule_qry[0])))

                # set rate of schedule
                self.__domain.Do(Set(Qry(schedule_qry[0]), 'rate', schedule[i][1]))

                scheduled_time_qry = self.__domain.Do(Get(Qry(schedule_qry[0]).Pth("scheduledTimes").Trm()))

                # if no schedule time exist then create a new one
                if not scheduled_time_qry:

                    scheduled_time_qry = self.__domain.Do(Cmp().Crn("http://www.oaam.de/oaam/model/v140/allocations", "ScheduledTime", 1))
                    self.__domain.Do(Add(Qry(schedule_qry[0]), 'scheduledTimes', Qry(scheduled_time_qry[0])))

                # set offset of schedule
                self.__domain.Do(Set(Qry(scheduled_time_qry[0]), 'startTime', schedule[i][0]))

    # PRINTING METHODS

    def print_tasks(self):

        print("Task List: ")

        for task in self.__tasks_qry:
            print("\t" + self.get_name(task) + "(" + str(self.__tasks_dic[str(task)]) + ")")

    def print_devices(self):

        print("Device List: ")

        for device in self.__devices_qry:
            print("\t" + self.get_name(device) + "(" + str(self.__devices_dic[str(device)]) + ")")

    def get_task_name(self, i):
        return self.get_name(self.__tasks_qry[i])

    def get_device_name(self, d):
        return self.get_name(self.__devices_qry[d])
