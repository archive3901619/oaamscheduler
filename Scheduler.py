import numpy as np
import gurobipy as gp
from gurobipy import GRB
import math


class Scheduler:

    __wcet = []
    __time_limit = 5
    __reset_time = 0

    # Status Codes
    OPTIMAL = 0
    TERMINATED = 1
    INFEASIBLE = 2

    __status = INFEASIBLE

    def __init__(self, avionics_model):

        self.model = avionics_model

        # save basic information
        # device count
        self.__d_n = self.model.get_device_count()

        # task count
        self.__t_n = self.model.get_task_count()

        # resource type count
        self.__r_n = self.model.get_resource_type_count()

        # retrieve precedence relationsships
        self.__precedence_matrix = self.model.get_precedence_matrix()

        # retrieve signal matrix
        self.__signal_matrix = self.model.get_signal_matrix()

        # retrieve wcet of a task for each device
        self.__wcets = self.model.get_wcet_matrix()

        # retrieve rates of each task
        self.__rates = self.model.get_task_rates()

        # Determine the possible software mappings aka capabilities
        self.__capabilities = self.model.get_capabilities()

# ----------------- SCHEDULING -----------------------------------

    def __add_resource_constraints(self, model_opt, tasks, indices):

        # get resource consumption and available resources
        A_resources, b_resources = self.model.get_resource_matrix()

        # add constraint for every device and every type of resource
        for d in range(self.__d_n):
            for r in range(self.__r_n):
                model_opt.addConstr(sum([tasks[i] * A_resources[i // self.__d_n][d][r] for i in indices if i % self.__d_n == d]) <=
                                    float(b_resources[d][r]), name="R" + str(r) + ",D"+str(d))

    def __add_unique_constraints(self, model_opt, tasks, indices):

        # add constraint for every task
        for t in range(self.__t_n):
            model_opt.addConstr(sum([tasks[i] for i in indices if i // self.__d_n == t]) == 1, name="U" + str(t))

    def __add_segregation_constraints(self, model_opt, tasks, indices):

        # apply user defined segregation constraints
        for s in self.model.get_segregation_pairs():
            for d in range(self.__d_n):
                if len([i for i in indices if i % self.__d_n == d and i // self.__d_n in s]) > 0:
                    model_opt.addConstr(sum([tasks[i] for i in indices if i % self.__d_n == d and i // self.__d_n in s]) <= 1, name="Seg"+str(s)+",D"+str(d))

    def __add_atomic_constraints(self, model_opt, tasks, indices):

        for a in self.model.get_atomic_pairs():

            # get atomic pair of tasks
            t1 = a[0]
            t2 = a[1]

            A_atomic = [0] * (self.__t_n * self.__d_n)

            for d in range(self.__d_n):
                A_atomic[t1 * self.__d_n + d] = d
                A_atomic[t2 * self.__d_n + d] = -d

            model_opt.addConstr(sum([tasks[i] * A_atomic[i] for i in indices if i // self.__d_n == t1 or i // self.__d_n == t2]) == 0,
                                name="Atomic"+str(t1)+","+str(t2))

    def __add_used_device_constraints(self, model_opt, tasks, devices, indices):

        # add constraint for every device
        for d in range(self.__d_n):
            model_opt.addConstr(sum([tasks[i] for i in indices if i % self.__d_n == d]) - self.__t_n * devices[d] <= 0, name="UD"+str(d)+",1")
            model_opt.addConstr(sum([tasks[i] for i in indices if i % self.__d_n == d]) - self.__t_n * devices[d] >= -self.__t_n + 1, name="UD"+str(d)+",2")

    def __add_non_overlapping_constraints(self, model_opt, tasks, offsets, couplings, indices, coupling_indices, non_coupling):

        # task pair with coupling variable can run on the same device
        # -> non overlapping constraint necessary
        for c in coupling_indices:
            
            # retrieve the index of the two tasks which can run on the same device
            i = c // self.__t_n
            j = c % self.__t_n

            # determine gcd of both task rates
            g_ij = 1000000 / np.lcm(int(1000000 * self.__rates[i]), int(1000000 * self.__rates[j]))

            model_opt.addConstr(couplings[c] >= math.floor(-1 / self.__rates[i] / g_ij), "E" + str(i) + "," + str(j) + ",1")
            model_opt.addConstr(couplings[c] <= math.floor(1 / self.__rates[j] / g_ij), "E" + str(i) + "," + str(j) + ",2")

            for d in range(self.__d_n):
                if i * self.__d_n + d in indices and j * self.__d_n + d in indices:

                    # Add non overlapping constraints for each device
                    model_opt.addConstr(self.__wcets[i][d] - 100 * (2 - tasks[i*self.__d_n+d] - tasks[j*self.__d_n+d]) <= (offsets[j] - offsets[i]) - g_ij * couplings[c], "NonOverlap" + str(i) + "," + str(j) + ",D"+str(d)+",1")
                    model_opt.addConstr((offsets[j] - offsets[i]) - g_ij * couplings[c] <= g_ij - self.__wcets[j][d] + 100 * (2 - tasks[i*self.__d_n+d] - tasks[j*self.__d_n+d]), "NonOverlap" + str(i) + "," + str(j) + ",D"+str(d)+",2")

        # segregate task pairs, that dont have a coupling variable
        for c in non_coupling:
            i = c // self.__t_n
            j = c % self.__t_n

            for d in range(self.__d_n):
                if i * self.__d_n + d in indices and j * self.__d_n + d in indices:
                    model_opt.addConstr(tasks[i * self.__d_n + d] + tasks[j * self.__d_n + d] <= 1, "Seg" + str(i) + " , " + str(j) + ",D"+str(d))

    def __add_communication_constraints(self, model_opt, tasks, offsets, indices, couplings, coupling_indices):

        for i in range(self.__t_n):
            for j in range(self.__t_n):
                if self.__precedence_matrix[i][j] > 0:

                    g_ij = 1000000 / np.lcm(int(1000000 * self.__rates[i]), int(1000000 * self.__rates[j]))

                    # check if coupling variable already exists
                    if i * self.__t_n + j in coupling_indices:
                        e_ij = couplings[i * self.__t_n + j]
                    else:
                        e_ij = model_opt.addVar(vtype=GRB.INTEGER, name="e"+str(i)+","+str(j))

                        model_opt.addConstr(e_ij >= math.floor(-1 / float(self.__rates[i]) / g_ij), "E" + str(i) + "," + str(j) + "1")
                        model_opt.addConstr(e_ij <= math.floor(1 / float(self.__rates[j]) / g_ij), "E" + str(i) + "," + str(j) + "2")

                    # get possible connections of tasks
                    connections = self.model.get_connections(i, j)

                    for c in connections:

                        wctt = self.model.get_signal_time(i, j, c[0], c[1])

                        # if forward edge
                        if self.__precedence_matrix[i][j] == 1 and i * self.__d_n + c[0] in indices and j * self.__d_n + c[1] in indices:
                            model_opt.addConstr(self.__wcets[i][c[0]] + wctt - 100 * (2 - tasks[i * self.__d_n + c[0]] - tasks[j * self.__d_n + c[1]]) <= (offsets[j] - offsets[i]) - g_ij * e_ij)
                        # take inverse communication constraint if backward edge
                        elif self.__precedence_matrix[i][j] == 2 and i * self.__d_n + c[0] in indices and j * self.__d_n + c[1] in indices:
                            model_opt.addConstr(self.__wcets[j][c[1]] + wctt - 100 * (2 - tasks[i * self.__d_n + c[0]] - tasks[j * self.__d_n + c[1]]) <= (offsets[i] - offsets[j]) - g_ij * e_ij)

    def __add_latency_constraints(self, model_opt, offsets, starting):

        for t in self.model.get_time_delay_restrictions():

            # retrieve information from time delay restriction
            start = t[0]
            end = t[1]
            delay = t[2]

            model_opt.addConstr(offsets[end] + starting[end] / self.__rates[end] - (offsets[start] + starting[start] / self.__rates[start]) <= delay, name="TimeDelay "+str(start)+","+str(end))

    def __add_synchronicity_constraints(self, model_opt, offsets):

        for s in self.model.get_synchronicity_restrictions():

            # get both tasks and maximal jitter
            i = s[0]
            j = s[1]
            jitter = s[2]

            model_opt.addConstr(offsets[j] - offsets[i] <= jitter, name="Syn"+str(i)+","+str(j))
            model_opt.addConstr(offsets[j] - offsets[i] >= jitter, name="Syn"+str(i)+","+str(j))

    def __add_starting_constraints(self, model_opt, offsets, tasks, indices, starting):

        for i in range(self.__t_n):
            for j in range(self.__t_n):
                if self.__precedence_matrix[i][j] == 1:
                    for d1 in range(self.__d_n):
                        if i * self.__d_n + d1 in indices:
                            for d2 in range(self.__d_n):
                                if j * self.__d_n + d2 in indices:
                                    wctt = self.model.get_signal_time(i, j, d1, d2)
                                    model_opt.addConstr(offsets[i] + starting[i] / self.__rates[i] + self.__wcets[i][d1] + wctt
                                                        - 100 * (2 - tasks[i * self.__d_n + d1] - tasks[j * self.__d_n + d2]) <= offsets[j] + starting[j] / self.__rates[j],
                                                        name="Starting " + str(i) + "," + str(j) + ",D"+str(d1) + "," + str(d2))

    def __get_coupling_indices(self, optimality, indices):

        # create groups
        same_rate_precedence = []
        same_rate = []
        harmonic_rate = []
        non_harmonic_rate = []
        excluded = []

        # classify task pairs
        for i in range(self.__t_n):
            for j in range(i + 1, self.__t_n):

                # check through capabilities if two tasks can be even on the same device
                no_same_device = True

                for d in range(self.__d_n):
                    if i * self.__d_n + d in indices and j * self.__d_n + d in indices:
                        no_same_device = False

                # see if they are excluded
                g_ij = 1000000 / np.lcm(int(1000000 * self.__rates[i]), int(1000000 * self.__rates[j]))

                if min(self.__wcets[i]) + min(self.__wcets[i]) > g_ij or no_same_device:
                    excluded.append(i * self.__t_n + j)
                else:
                    if (self.__precedence_matrix[i][j] == 1 or self.__precedence_matrix[j][i] == 1) and (
                            self.__rates[i] == self.__rates[j]):
                        same_rate_precedence.append(i * self.__t_n + j)
                    elif self.__rates[i] == self.__rates[j]:
                        same_rate.append(i * self.__t_n + j)
                    elif self.__rates[i] % self.__rates[j] == 0 or self.__rates[j] % self.__rates[i] == 0:
                        harmonic_rate.append(i * self.__t_n + j)
                    else:
                        non_harmonic_rate.append(i * self.__t_n + j)

        # concetenate all lists by priority
        task_pairs = same_rate_precedence + same_rate + harmonic_rate + non_harmonic_rate + excluded

        # determine index at which the list splits
        coupling_limit = int(len(task_pairs) * optimality)

        return task_pairs[:coupling_limit], task_pairs[coupling_limit:len(task_pairs)]

# -------------------- POSTPROCESSING --------------------------------------

    def __add_starting_constraints_post(self, model_schedule, offsets, starts, task_assignment):

        for i in range(self.__t_n):
            for j in range(self.__t_n):

                # same device no signal time
                if self.__precedence_matrix[i][j] == 1 and task_assignment[i] == task_assignment[j]:
                    model_schedule.addConstr(offsets[i] + starts[i] / self.__rates[i] + self.__wcet[i] <= offsets[j] + starts[j] / self.__rates[j], name="Post: Starting " + str(i) + ", " + str(j))

                # different device signal time
                elif self.__precedence_matrix[i][j] == 1 and task_assignment[i] != task_assignment[j]:
                    wctt = self.model.get_signal_time(i, j, task_assignment[i], task_assignment[j])
                    model_schedule.addConstr(offsets[i] + starts[i] / self.__rates[i] + self.__wcet[i] + wctt <= offsets[j] + starts[j] / self.__rates[j], name="Post: Starting " + str(i) + ", " + str(j))

    def __add_communication_constraints_post(self, model_schedule, offsets, task_assignment):

        for i in range(self.__t_n):
            for j in range(self.__t_n):
                if self.__precedence_matrix[i][j] > 0 and task_assignment[i] != task_assignment[j]:

                    g_ij = 1000000 / np.lcm(int(1000000 * self.__rates[i]), int(1000000 * self.__rates[j]))
                    e_ij = model_schedule.addVar(vtype=GRB.INTEGER)

                    # get transmission time between devices
                    wctt = self.model.get_signal_time(i, j, task_assignment[i], task_assignment[j])

                    model_schedule.addConstr(e_ij >= math.floor(- 1 / self.__rates[i] / g_ij), "PostCom: E" + str(i) + "," + str(j) + ",1")
                    model_schedule.addConstr(e_ij <= math.floor(1 / self.__rates[i] / g_ij), "Post: E" + str(i) + "," + str(j) + ",2")

                    if self.__precedence_matrix[i][j] == 1:  # forward edge
                        model_schedule.addConstr(float(self.__wcet[i] + wctt) <= (offsets[j] - offsets[i]) - g_ij * e_ij, name="Post: Comm"+str(i)+","+str(j)+",1")

                    elif self.__precedence_matrix[i][j] == 2:  # backward edge
                        model_schedule.addConstr(float(self.__wcet[j] + wctt) <= (offsets[i] - offsets[j]) - g_ij * e_ij, name="Post: Comm"+str(i)+","+str(j)+",2")

    def __add_latency_constraints_post(self, model_schedule, offsets, starting):

        for t in self.model.get_time_delay_restrictions():

            # retrieve information from time delay restriction
            start = t[0]
            end = t[1]
            delay = t[2]

            model_schedule.addConstr(offsets[end] + starting[end] / self.__rates[end] - (offsets[start] + starting[start] / self.__rates[start]) <= delay, name="Post: TimeDelay" + str(start) + "," + str(end))

    def __add_non_overlapping_constraints_post(self, model_schedule, offsets, task_assignment):

        # Add non overlapping constraints for each device
        for d in range(self.__d_n):

            # get tasks of device
            tasks = []
            for i in range(self.__t_n):
                if task_assignment[i] == d:
                    tasks.append(i)

            # create non overlapping constraints fpr each possible pair of tasks
            for i in range(len(tasks) - 1):
                for j in range(i + 1, len(tasks)):

                    # calculate common divisor of periods
                    g_ij = 1000000 / np.lcm(int(1000000 * self.__rates[tasks[i]]), int(1000000 * self.__rates[tasks[j]]))

                    # add new auxillary variable
                    e_ij = model_schedule.addVar(vtype=GRB.INTEGER, name="e" + str(tasks[i]) + "," + str(tasks[j]))

                    # Add non overlapping constraints
                    model_schedule.addConstr(self.__wcet[tasks[i]] <= (offsets[tasks[j]] - offsets[tasks[i]]) - g_ij * e_ij, name="Post: NonOverlapping" + str(i) + "," + str(j) + ",1")
                    model_schedule.addConstr((offsets[tasks[j]] - offsets[tasks[i]]) - g_ij * e_ij <= g_ij - self.__wcet[tasks[j]], "Post: NonOverlapping" + str(i) + "," + str(j) + ",2")
                    model_schedule.addConstr(e_ij >= math.floor((self.__wcet[tasks[i]] - 1 / self.__rates[tasks[i]]) / g_ij), "Post: E" + str(i) + "," + str(j) + ",1")
                    model_schedule.addConstr(e_ij <= math.floor((1 / self.__rates[tasks[j]] - self.__wcet[tasks[j]]) / g_ij), "Post: E" + str(i) + "," + str(j) + ",2")

    def __add_synchronicity_constraints_post(self, model_schedule, offsets):

        for s in self.model.get_synchronicity_restrictions():

            # read values
            i = s[0]
            j = s[1]
            jitter = s[2]

            model_schedule.addConstr(offsets[j] - offsets[i] <= jitter, name="Syn" + str(i) + "," + str(j))
            model_schedule.addConstr(offsets[j] - offsets[i] >= jitter, name="Syn" + str(i) + "," + str(j))

# ------------------- DYNAMIC SCHEDULING -------------------------------------

    def __get_indices(self, task_list, task_assignment, device_exclusion):

        indices_mask = [0] * (self.__t_n * self.__d_n)

        # check for possible capabilities
        for i in task_list:
            for d in range(self.__d_n):
                if self.__capabilities[i * self.__d_n + d] != -1 and d not in device_exclusion:
                    indices_mask[i * self.__d_n + d] = 1

        # check for possible exclusions through segregation constraints
        for s in self.model.get_segregation_pairs():
            if task_assignment[s[0]] == -1 and task_assignment[s[1]] != -1:
                indices_mask[int(s[0] * self.__d_n + task_assignment[int(s[1])])] = 0
            elif task_assignment[s[1]] == -1 and task_assignment[s[0]] != -1:
                indices_mask[int(s[1] * self.__d_n + task_assignment[int(s[0])])] = 0

        # check for possible exclusions through non overlapping constraints
        for i in task_list:
            for j in range(self.__t_n):
                g_ij = 1000000 / np.lcm(int(1000000 * self.__rates[i]), int(1000000 * self.__rates[j]))
                if min(self.__wcets[i]) + min(self.__wcets[j]) > g_ij:
                    indices_mask[int(i * self.__d_n + task_assignment[j])] = 0

        # add fixed task assignments as single variables
        for i in range(self.__t_n):
            if task_assignment[i] != -1:
                indices_mask[i * self.__d_n + task_assignment[i]] = 1

        return [i for i in range(len(indices_mask)) if indices_mask[i] == 1]

# ----------------------------------------------------------------------------

    def schedule(self, optimality=1, time_limit=-1, print_out=False):

        # set time limit
        self.__time_limit = time_limit

        # set the model status
        self.__status = self.INFEASIBLE

        if not self.model.is_loaded():
            print("Cannot perform scheduling because Model is not loaded.")
            return False, []

        model_opt = gp.Model("MIP function alloction")

        # cancel model log information
        if not print_out:
            model_opt.Params.LogToConsole = 0

        # Determine optimization variabales as indices from capabilities
        indices_opt = [i for i in range(self.__t_n * self.__d_n) if self.__capabilities[i] != -1]
        coupling_indices, non_coupling_indices = self.__get_coupling_indices(optimality, indices_opt)

        # Create variable names
        task_assignment_names = ["T"+str(i // self.__d_n)+"D"+str(i % self.__d_n) for i in indices_opt]
        device_names = ["DU" + str(i) for i in range(self.__d_n)]
        offset_names = ["a" + str(i) for i in range(self.__t_n)]
        starting_names = ["k" + str(i) for i in range(self.__t_n)]
        coupling_names = ["e"+str(i // self.__t_n)+","+str(i % self.__t_n) for i in coupling_indices]

        # Add all necessary variables
        task_variables = model_opt.addVars(indices_opt, vtype=GRB.BINARY, name=task_assignment_names)
        device_variables = model_opt.addVars([i for i in range(self.__d_n)], vtype=GRB.BINARY, name=device_names)
        offsets_var = model_opt.addVars([i for i in range(self.__t_n)], vtype=GRB.CONTINUOUS, lb=[0] * self.__t_n, ub=[1 / self.__rates[i] for i in range(self.__t_n)], name=offset_names)
        starting_var = model_opt.addVars([i for i in range(self.__t_n)], vtype=GRB.INTEGER, lb=[0] * self.__t_n, name=starting_names)
        coupling_var = model_opt.addVars(coupling_indices,  vtype=GRB.INTEGER, name=coupling_names)

        # set the model objective to minimize device count
        model_opt.setObjective(device_variables.sum(), GRB.MINIMIZE)

        # Adding constraints for task allocation
        self.__add_unique_constraints(model_opt, task_variables, indices_opt)
        self.__add_used_device_constraints(model_opt, task_variables, device_variables, indices_opt)
        self.__add_segregation_constraints(model_opt, task_variables, indices_opt)
        self.__add_atomic_constraints(model_opt, task_variables, indices_opt)
        self.__add_resource_constraints(model_opt, task_variables, indices_opt)

        # Adding constraints for scheduling
        self.__add_non_overlapping_constraints(model_opt, task_variables, offsets_var, coupling_var, indices_opt, coupling_indices, non_coupling_indices)
        self.__add_communication_constraints(model_opt, task_variables, offsets_var, indices_opt, coupling_var, coupling_indices)
        self.__add_latency_constraints(model_opt, offsets_var, starting_var)
        self.__add_synchronicity_constraints(model_opt, offsets_var)
        self.__add_starting_constraints(model_opt, offsets_var, task_variables, indices_opt, starting_var)

        # define termination rule for optimization
        def terminate(model, where):
            if where == GRB.Callback.MIPSOL:
                self.__reset_time = model.cbGet(GRB.Callback.RUNTIME)
            elif where == GRB.Callback.MIP:
                if self.__time_limit >= 0:
                    if model.cbGet(GRB.Callback.RUNTIME) - self.__reset_time >= self.__time_limit:
                        self.__status = self.TERMINATED
                        model.terminate()

        # Optimize the model
        model_opt.optimize(terminate)

        # if Model not feasible then cancel function
        if model_opt.status == GRB.INFEASIBLE or model_opt.SolCount == 0:

            # compute irreducable inconsistent subset
            # save the file consiting of the reasons why the model failed
            model_opt.computeIIS()
            model_opt.write("model.ilp")

            # return zero values since optimization was not succesful
            return self.__status, [], []

        # construct task_assignment_vector
        task_assignment = []

        # retrieve task assignment from solution
        for i in indices_opt:
            if round(task_variables[i].x) == 1:
                task_assignment.append(int(i % self.__d_n))

        # POSTPROCESSING
        # --------------

        # create new model for postprocessing
        model_schedule = gp.Model("MIP Scheduling")

        if not print_out:
            model_schedule.Params.LogToConsole = 0

        # add variables
        offsets_scheduling_var = model_schedule.addVars([i for i in range(self.__t_n)], vtype=GRB.CONTINUOUS, lb=[0] * self.__t_n,
                                                        ub=[1 / self.__rates[i] for i in range(self.__t_n)], name=offset_names)
        starting_var = model_schedule.addVars([i for i in range(self.__t_n)], vtype=GRB.INTEGER, lb=[0] * self.__t_n, ub=[10] * self.__t_n, name=starting_names)

        # since task assignment is clear, determine real wcet of each task
        self.__wcet = [float(self.__wcets[i][task_assignment[i]]) for i in range(self.__t_n)]

        # set objective function to minimize average termination time
        model_schedule.setObjective(sum([offsets_scheduling_var[i] + starting_var[i] / self.__rates[i] for i in range(self.__t_n)]), GRB.MINIMIZE)

        # add constraints for scheduling
        self.__add_non_overlapping_constraints_post(model_schedule, offsets_scheduling_var, task_assignment)
        self.__add_communication_constraints_post(model_schedule, offsets_scheduling_var, task_assignment)
        self.__add_latency_constraints_post(model_schedule, offsets_scheduling_var, starting_var)
        self.__add_synchronicity_constraints_post(model_schedule, offsets_scheduling_var)
        self.__add_starting_constraints_post(model_schedule, offsets_scheduling_var, starting_var, task_assignment)

        # solve model
        model_schedule.optimize(terminate)

        # if Model not feasible then cancel function
        if model_schedule.status == GRB.INFEASIBLE or model_schedule.SolCount == 0:

            # give feed back why model failed
            model_schedule.computeIIS()
            model_schedule.write('model.ilp')
            return self.__status, [], []

        elif self.__status == GRB.OPTIMAL:
            self.__status = self.OPTIMAL

        # RESULTS
        # -------

        schedule = [[-1, -1, -1]] * self.__t_n

        # create scheduling solution
        for i in range(self.__t_n):
            schedule[i] = [offsets_scheduling_var[i].x + starting_var[i].x / self.__rates[i], self.__rates[i], self.__wcet[i]]

        # print solution if necessary
        if print_out:

            # Print out solution
            tasks = self.model.get_task_list()
            devices = self.model.get_device_list()

            print("Task Assignments + Start Time: --------------")

            for i in indices_opt:
                if round(task_variables[i].x == 1):
                    print(self.model.get_name(tasks[i // self.__d_n]) + " -> " + self.model.get_name(devices[i % self.__d_n])
                          + " , " + str(round(schedule[i // self.__d_n][0], 6)) + "s")

            print()
            print("Used Devices: --------------")

            for i in range(self.__d_n):
                if round(device_variables[i].x == 1):
                    print(self.model.get_name(devices[i]) + " used")

        return self.__status,  task_assignment, schedule

    def dynamic_schedule(self, optimality=1, task_list=None, device_exclusion=None, time_limit=-1, print_out=True):

        if task_list is None:
            task_list = []

        if device_exclusion is None:
            device_exclusion = []

        # reschedule the tasks from task_list on devices that are not in device_exclusion
        # set time limit
        self.__time_limit = time_limit

        # set the model status
        self.__status = self.INFEASIBLE

        if not self.model.is_loaded():
            print("Cannot perform scheduling because Model is not loaded.")
            return False, []

        # save old devices
        old_devices = [-1] * self.__t_n

        # retrieve current task assignment
        task_assignment = self.model.get_task_assignment()

        # add to the task list all tasks without task assignment and
        # add to the tasks list all tasks that need to be rescheduled due to device exclusion
        for i in range(len(task_assignment)):
            if task_assignment[i] == -1 or task_assignment[i] in device_exclusion:
                task_list.append(i)

        task_list = np.unique(task_list)

        # clear out the task assignment for tasks that have to be rescheduled
        for i in task_list:
            old_devices[i] = task_assignment[i]
            task_assignment[i] = -1

        # create a model
        model_opt = gp.Model("Rescheduling")

        # cancel model log information
        if not print_out:
            model_opt.Params.LogToConsole = 0

        # create a list possible task assignments
        indices_opt = self.__get_indices(task_list, task_assignment, device_exclusion)
        coupling_indices, non_coupling_indices = self.__get_coupling_indices(optimality, indices_opt)

        # create variable names
        task_assignment_names = ["T" + str(i // self.__d_n) + "D" + str(i % self.__d_n) for i in indices_opt]
        device_names = ["DU" + str(i) for i in range(self.__d_n)]
        offset_names = ["a" + str(i) for i in range(self.__t_n)]
        starting_names = ["k" + str(i) for i in range(self.__t_n)]
        coupling_names = ["e" + str(i // self.__t_n) + "," + str(i % self.__t_n) for i in coupling_indices]

        # add variables
        task_variables = model_opt.addVars(indices_opt, vtype=GRB.BINARY, name=task_assignment_names)
        offsets_var = model_opt.addVars([i for i in range(self.__t_n)], vtype=GRB.CONTINUOUS, lb=[0] * self.__t_n, ub=[1 / self.__rates[i] for i in range(self.__t_n)], name=offset_names)
        starting_var = model_opt.addVars([i for i in range(self.__t_n)], lb=[0] * self.__t_n, name=starting_names)
        device_variables = model_opt.addVars([i for i in range(self.__d_n)], vtype=GRB.BINARY, name=device_names)
        coupling_var = model_opt.addVars(coupling_indices, vtype=GRB.INTEGER, name=coupling_names)

        # set objective
        model_opt.setObjective(device_variables.sum(), GRB.MINIMIZE)

        # Adding constraints for task allocation
        self.__add_unique_constraints(model_opt, task_variables, indices_opt)
        self.__add_used_device_constraints(model_opt, task_variables, device_variables, indices_opt)
        self.__add_segregation_constraints(model_opt, task_variables, indices_opt)
        self.__add_atomic_constraints(model_opt, task_variables, indices_opt)
        self.__add_resource_constraints(model_opt, task_variables, indices_opt)

        # Adding constraints for scheduling
        self.__add_non_overlapping_constraints(model_opt, task_variables, offsets_var, coupling_var, indices_opt, coupling_indices, non_coupling_indices)
        self.__add_communication_constraints(model_opt, task_variables, offsets_var, indices_opt, coupling_var, coupling_indices)
        self.__add_latency_constraints(model_opt, offsets_var, starting_var)
        self.__add_synchronicity_constraints(model_opt, offsets_var)
        self.__add_starting_constraints(model_opt, offsets_var, task_variables, indices_opt, starting_var)

        # define termination rule for optimization
        def terminate(model, where):
            if where == GRB.Callback.MIPSOL:
                self.__reset_time = model.cbGet(GRB.Callback.RUNTIME)
            elif where == GRB.Callback.MIP:
                if self.__time_limit >= 0:
                    if model.cbGet(GRB.Callback.RUNTIME) - self.__reset_time >= self.__time_limit:
                        self.__status = self.TERMINATED
                        model.terminate()

        # Optimize the model
        model_opt.optimize(terminate)

        # if Model not feasible then cancel function
        if model_opt.status == GRB.INFEASIBLE or model_opt.SolCount == 0:
            model_opt.computeIIS()
            model_opt.write("model.ilp")
            return self.__status, [], []

        # construct task_assignment_vector
        task_assignment = []

        # retrieve task assignment from solution
        for i in indices_opt:
            if round(task_variables[i].x) == 1:
                task_assignment.append(int(i % self.__d_n))

        # POSTPROCESSING
        # --------------

        # create new model for postprocessing
        model_schedule = gp.Model("MIP Scheduling")

        if not print_out:
            model_schedule.Params.LogToConsole = 0

        # add variables
        offsets_scheduling_var = model_schedule.addVars([i for i in range(self.__t_n)], vtype=GRB.CONTINUOUS, lb=[0] * self.__t_n, ub=[1 / self.__rates[i] for i in range(self.__t_n)], name=offset_names)
        starting_var = model_schedule.addVars([i for i in range(self.__t_n)], vtype=GRB.INTEGER, lb=[0] * self.__t_n, name=starting_names)

        # since task assignment is clear, determine real wcet of each task
        self.__wcet = [float(self.__wcets[i][task_assignment[i]]) for i in range(self.__t_n)]

        # set objective function to minimize average starting time
        model_schedule.setObjective(sum([offsets_scheduling_var[i] + starting_var[i] / self.__rates[i] for i in range(self.__t_n)]), GRB.MINIMIZE)

        # add constraints for scheduling
        self.__add_non_overlapping_constraints_post(model_schedule, offsets_scheduling_var, task_assignment)
        self.__add_communication_constraints_post(model_schedule, offsets_scheduling_var, task_assignment)
        self.__add_latency_constraints_post(model_schedule, offsets_scheduling_var, starting_var)
        self.__add_synchronicity_constraints_post(model_schedule, offsets_scheduling_var)
        self.__add_starting_constraints_post(model_schedule, offsets_scheduling_var, starting_var, task_assignment)

        # solve model
        model_schedule.optimize(terminate)

        # if Model not feasible then cancel function
        if model_schedule.status == GRB.INFEASIBLE or model_schedule.SolCount == 0:

            # give feed back why model failed
            model_schedule.computeIIS()
            model_schedule.write('model.ilp')
            return self.__status, [], []

        elif self.__status == GRB.OPTIMAL:
            self.__status = self.OPTIMAL

        # RESULTS
        # -------

        schedule = [[-1 - 1 - 1]] * self.__t_n

        # create scheduling solution
        for i in range(self.__t_n):
            schedule[i] = [offsets_scheduling_var[i].x + starting_var[i].x / self.__rates[i], self.__rates[i], self.__wcet[i]]

        # print solution if necessary
        if print_out:

            # Print out solution
            tasks = self.model.get_task_list()
            devices = self.model.get_device_list()

            print("Task Assignments + Start Time: --------------")

            for i in indices_opt:
                if round(task_variables[i].x == 1):
                    print(self.model.get_name(tasks[i // self.__d_n]) + " -> " + self.model.get_name(devices[i % self.__d_n])
                          + " , " + str(round(schedule[i // self.__d_n][0], 6)) + "s")

            print()
            print("New Task Assignments: --------------------")

            for i in task_list:
                old_device_name = self.model.get_name(devices[old_devices[i]]) if old_devices[i] != -1 else ""
                print(self.model.get_name(tasks[i]), ":", old_device_name, "->", self.model.get_name(devices[task_assignment[i]]))

            print()
            print("Used Devices: --------------")

            for i in range(self.__d_n):
                if round(device_variables[i].x == 1):
                    print(self.model.get_name(devices[i]) + " used")

        return self.__status, task_assignment, schedule
